import firebase from 'firebase/app';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyAdQP2UvGKTFhSqzeFVXSs5w1IDDNd3lIk',
  authDomain: 'timeline-5f3a2.firebaseapp.com',
  databaseURL: 'https://timeline-5f3a2.firebaseio.com',
  projectId: 'timeline-5f3a2',
  storageBucket: 'timeline-5f3a2.appspot.com',
  messagingSenderId: '239010607051'
};

firebase.initializeApp(config);

export default firebase;
