import firebase from './firebaseConfig';
import { sampleItem, groupToInit } from '../components/settings';

var storedEmail = JSON.parse(localStorage.getItem('TL_email'));
let email = storedEmail;

const db = firebase.firestore();

const addUser = (user) => {
  if(user){
    email = user.email;

    localStorage.setItem('TL_email', JSON.stringify(email));

    const userData = {
      displayName: user.displayName,
      photoURL: user.photoURL,
      lastLogIn: Date.now()
    };

    db.collection('users').doc(user.email).set({...userData}, { merge: true })
      .then(function() {
        console.log('FB --- User Saved to FireBase');
      })
      .catch(function(error) {
        console.error('FB - addUser - Error writing document: ', error);
      });
  }
};

const getAllGrous = () => {
  let docRef = db.collection('groups').doc(email);
  return docRef.get().then(function(doc) {
    if (doc.exists) {
      let groups = doc.data();
      const groupsArr = [];
      for (let key in groups) {
        if (groups.hasOwnProperty(key)) {
            groupsArr.push(groups[key])
        }
      }
      return groupsArr
    } else {
      console.log('FB - NO Group exist!');
    }
  }).catch(function(error) {
    console.log('FB - getAllGrous - Error getting groups document:', error);
  });
};

const getAllItems = () => {
  let docRef = db.collection('items').doc(email);
  return docRef.get().then(function(doc) {
    if (doc.exists) {
      let items = doc.data();
      const itemsArr = [];
      for (let key in items) {
        if (items.hasOwnProperty(key)) {
          itemsArr.push(items[key])
        }
      }
      return itemsArr

    } else {
      console.log('FB - NO Item exist!');
    }
  }).catch(function(error) {
    console.log('FB - getAllItems - Error getting items document:', error);
  });
};

const updateGroups = (groups) => {
  db.collection('groups').doc(email).set({...groups});
};

const addItem = (item) => {
  if (item.start && item.end) { // Do not save item without start/end date to prevent error on TimeLine load
    db.collection('items').doc(email).set({[item.id]: item}, { merge: true })
    .then(function() {
      console.log('FB - Item Added!');
      window.location.reload();
    })
    .catch(function(error) {
      console.error('FB - addItem - Error writing document: ', error);
    });
  } else {
    console.log("Wrong Item obj, no dates exist!");
  }
};

const editItem = (item, reloadPage) => { // TODO: use it when move item also
  if (item.start && item.end) { // Do not save item without start/end date to prevent error on TimeLine load
    let docRef = db.collection('items').doc(email);
    docRef.get().then(function(doc) {
      if (doc.exists) {
        const itemToEdit = {...item};

        // Save edited item
        db.collection('items').doc(email).set({[item.id]: itemToEdit}, { merge: true })
          .then(function() {
            if(reloadPage && reloadPage.reload){ // Reload page only when save/edit item, not move
              window.location.reload();
            }
          })
          .catch(function(error) {
            console.error('FB - Error - Edited Item: ', error);
          });

      } else {
        console.log("FB - No Item to Edit!");
      }
    }).catch(function(error) {
      console.log("FB - editItem - Error getting document:", error);
    });
  } else {
    console.log("Wrong Item obj, no dates exist!");
  }
};
  
const updateItems = (items) => { // remove items when remove group
  // TODO: optimize to not replace whale data, just remove by key if it has id to remove

  // Convert redux to firestore items structure
  let itemsArrToSend = {};
  for(let i = 0; i < items.length; i++ ){
    let itemId = items[i].id;
    itemsArrToSend[itemId] = items[i];
  }

  db.collection('items').doc(email).set(itemsArrToSend);
};

const removeItem = (itemId) => {
  const items = db.collection('items').doc(email);
  items.update({
    [itemId]: firebase.firestore.FieldValue.delete()
  })
    .then(function() {
      console.log("FB - Item Removed!");
      window.location.reload(); // TODO: reload needs only to close modal. TODO - close modal without reload
    })
    .catch(function(error) {
      console.error("FB - removeItem - Error writing document: ", error);
    });
};

const addTermsAcceptance = (isAccepted) => {
  db.collection('users').doc(email).set({'isTermsAccepted': isAccepted}, { merge: true })
    .then(function() {
      console.log("FB - Terms Accepted!");
    })
    .catch(function(error) {
      console.error("FB - addTermsAcceptance - Error writing document: ", error);
    });
};


const removeAccount = (callback) => {
  const removeUser = () => {
    db.collection("users").doc(email).delete().then(function() {
      callback(); // LogOut
    }).catch(function(error) {
        console.error("Error removing User: ", error);
    });
  };

  const removeAllGroups = () => {
    db.collection("groups").doc(email).delete().then(function() {
      removeUser();
    }).catch(function(error) {
        console.error("Error removing All Groups: ", error);
    });
  };

  const removeItems = () => {
    db.collection("items").doc(email).delete().then(function() {
      removeAllGroups();
    }).catch(function(error) {
        console.error("Error removing Items: ", error);
    });
  };

  removeItems();
};



export {
  addUser,
  updateGroups,
  addItem,
  editItem,
  getAllGrous,
  getAllItems,
  updateItems,
  removeItem,
  addTermsAcceptance,
  removeAccount
};



// -- Get item with id
// let itemToEdit = doc.data()[item.id];

// -- Get all data
// db.collection('items')
//     .get()
//     .then(function(querySnapshot) {
//         querySnapshot.forEach(function(doc) {
//             console.log(doc.id, " => ", doc.data());
//         });
//     });


// db.collection('items').where(item.id, '==', item.id) // item.id = item
//     .get()
//     .then(function(querySnapshot) {
//         querySnapshot.forEach(function(doc) {
//             console.log(doc.id, " ======> ", doc.data());
//         });
//     })
//     .catch(function(error) {
//         console.log("Error getting documents: ", error);
//     });

// db.collection('items').doc(email).collection(item.id)
//             .get()
//             .then(function(querySnapshot) {
//                 console.log("============ 3 =============", querySnapshot)
//                 querySnapshot.forEach(function(doc) {
//                     console.log(doc.id, " ======> ", doc.data());
//                 });
//             })

// db.collection('data').doc(email)
//     .get()
//     .then(function(doc) {
//         doc.forEach(function(i) {
//             console.log(i.id, " => ", i.data());
//             // Build doc ref from doc.id
//             // db.collection('data').doc(doc.id).update({foo: "bar"});
//         });
//     })

// Add "item" document to colection "data"
// db.collection('data').doc(email).update({
//     items: firebase.firestore.FieldValue.arrayUnion(item)
// })

//.update({items: {item}    })
// db.collection('data').doc(email).update({'items': {item}    })//.set({items: item}, { merge: true })
// const userRef = db.collection('users').add({
//     email: 'aX'
// });


// Send Temperature param to firebase (no loop)
// function uppdateTemperature(tempParam){
//     let uid = modelController.storage.getUserData().uid;
//     DB.collection('settings').where("userId", "==", uid).get().then(function(doc){
//         // Get Doc Id
//         let docId = doc.docs[0].id;
//         // Sett climate value
//         DB.collection('settings').doc(docId).set(
//             {temp: tempParam},
//             { merge: true }
//         )
//     })
// }
