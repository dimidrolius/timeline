import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import 'typeface-roboto';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


// import { createStore, combineReducers, applyMiddleware } from "redux";
// import { createLogger } from "redux-logger";
//
// const initialState = {
//     result: 1,
//     lastValues: []
// };
//
// const mathReducer = (state = initialState, action) => {
//     switch (action.type) {
//         case "ADD":
//             state = {
//                 ...state,
//                 result: state.result + action.payload,
//                 lastValues: [...state.lastValues, action.payload]
//             };
//             break;
//         case "SUBTRACT":
//             state = {
//                 ...state,
//                 result: state.result - action.payload
//             };
//             break;
//         default:
//             return state;
//     }
//     return state;
// };
//
// const userReducer = (state = {
//     name: "Max",
//     age: 27
// }, action) => {
//     switch (action.type) {
//         case "SET_AGE":
//             state = {
//                 ...state,
//                 age: action.payload
//             };
//             break;
//         default:
//             return state;
//     }
//     return state;
// };
//
// const myLogger = (store) => (next) => (action) => {
//   console.log("Logged Action: ", action);
//     next(action);
// };
//
// const store = createStore(
//     combineReducers({mathReducer, userReducer}),
//     {},
//     applyMiddleware(myLogger, createLogger()));
//
// store.subscribe(() => {
//     console.log("Store Updated! ", store.getState());
// });
//
// store.dispatch({
//     type: "ADD",
//     payload: 10 // Value I want to change
// });
//
// store.dispatch({
//     type: "SUBTRACT",
//     payload: 10
// });
//
// store.dispatch({
//     type: "SET_AGE",
//     payload: 30
// });