import { editItem, addItem, removeItem, updateItems } from '../firebase/mutations';

const initialState = {
  items: [
    // {
    //   id: 0,
    //   start: new Date(2019, 7, 15),
    //   end: new Date(2019, 8, 2),
    //   content: 'Item to load Timeline', // Required to init TimeLine
    //   group: 0
    //   // types: 'box'
    // }
  ]
};

const itemsReducer = (state = initialState, action) => {
  switch (action.type){

    case 'LOADITEMS':
      if(action.payload){
        /* --
        // Firestore save start/end data as timestamp.
        // Timeline require timestamp to be converted in date format
        --*/
        let dataArrForTimeline = [];
        for (let i=0; i < action.payload.length; i++) {
            let item = action.payload[i];
            if (action.payload[i].start.hasOwnProperty('seconds')){ // Item exis on Server !--to fix--!
                item.start = action.payload[i].start.toDate();
                item.end = action.payload[i].end ? action.payload[i].end.toDate() : null; // ternary needs if item type is "point", not "range"
                item.type = action.payload[i].end ? 'range' : 'point';
            } else{                                                  // No items in firestor, load init item with date instead of timestamp
                item.start = action.payload[i].start;
                item.end = action.payload[i].end ? action.payload[i].end : null;
                item.type = action.payload[i].end ? 'range' : 'point';
            }

            dataArrForTimeline.push(item);
        }

        state = {
            // items: action.payload
            items: dataArrForTimeline
        };
      }
      break;

      case 'ADDITEM':
        for (let i = 0; i < state.items.length; i++) {
          if (state.items[i].id === action.payload.id) {    // Item exist in redux - edit mode (time will be edited when "Move Item") 
          let existItem = state.items[i];
            existItem = {...action.payload}
            // existItem.content = action.payload.content;
            // existItem.imgLink = action.payload.imgLink;
            // existItem.textEditor = action.payload.textEditor;
            // existItem.color = action.payload.color;
            // existItem.hideEndDate = action.payload.hideEndDate;

            // state.items.splice(i,1);
            editItem(existItem, {reload: true});
            break;
          }
          if (i === state.items.length - 1) {              // Haven't fount in redux - create New Item
            state.items = [...state.items, action.payload];

            addItem(action.payload);
            break;
          }
        }

        // return Object.assign({}, state, {
        //     ...state,
        //     items: [...state.items]
        // });
        break;

      case 'REMOVEITEM':
        const itemId = action.payload;
        let filtered = [];
        let itemCounter = 0;

        for(let stateItem of state.items){
          itemCounter ++;
          if(stateItem.id === itemId){
            filtered = state.items.filter(item => item !== stateItem);

            // Next lines should be inside to avoid writing empty "filtered" if create item and remove it without saving
            state = {
              items: [...filtered]
            };
            removeItem(itemId);
            break;
          }
          // if iterate all items and no item with id found (item created in timeline but not saved)
          // save existing items to reload timeline (created and not saved item will be removed from timeline)
          if(itemCounter === state.items.length){
            state = {
              items: [...state.items]
            };
          }
        }

        break;

        case 'UPLOADITEMS':
          // replace stringified dates by Date object
          const convertedData = action.payload.map((item) => {
            item.start = new Date(item.start);
            item.end = new Date(item.end);
            return item;
          })

          state = {
            items: convertedData
          };

        break;

        case 'REMOVEGROUPITEMS': // When remove group, items need to be removed also
          let filteredItems = [];
          for(let i = 0; i < state.items.length; i++){
            if(state.items[i].group === +action.payload){
              if(filteredItems.length !== 0){
                filteredItems = filteredItems.filter(item => item !== state.items[i]);
              }else{
                filteredItems = state.items.filter(item => item !== state.items[i]); // Find item first time
              }
            }
          }

          if(filteredItems.length !== 0){
            state = {
              items: [...filteredItems]
            };
          }

          updateItems(state.items);
          break;

        default:
            return state
    }
    return state;
};

export default itemsReducer;
