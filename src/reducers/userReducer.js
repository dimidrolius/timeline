import { addUser, addTermsAcceptance } from '../firebase/mutations';

let initialState = {
  user: null,
  isTermsAccept: false
};

const userReducer = (state = initialState, action) => {

  switch (action.type){
    case 'ADDUSER':
      state = {
        ...state, user: action.payload
      };

      let storedEmail = JSON.parse(localStorage.getItem('TL_email'));

      // Store data only if no email in localStorage
      if(storedEmail == null){
        addUser(state.user);
      }
      break;

    case 'SETTERMS':
      state = {
        ...state, isTermsAccept: action.payload
      };
      addTermsAcceptance(state.isTermsAccept);
      break;

    default:
      return state
  }
    return state;
};

export default userReducer;

