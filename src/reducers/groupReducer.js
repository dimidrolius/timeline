import { updateGroups } from '../firebase/mutations';

let initialState = {
  groups: [
    {
      id: 0,
      content: 'Group to load Timeline'
      // title: 'Group title',
      // nestedGroups: [2]
    }
  ]
};

const updateGroupHandler = (state) => {
  updateGroups(state.groups);
};

const groupReducer = (state = initialState, action) => {
  switch (action.type){
    case 'LOADGROUPS':
      // Load groups from firestore
      if(action.payload){
          state = {
              groups: action.payload
          };
      }
      break;

    case 'ADDGROUP':
      // Add nested group if parent selected
      if(action.payload.parentGroup){
        const parentId = +action.payload.parentGroup.id;
        const nestedGroup = action.payload.parentGroup.nestedGroup;

        for(let i = 0; i < state.groups.length; i++) {
          if(state.groups[i].id === parentId) {
            if(state.groups[i].hasOwnProperty('nestedGroups')){ // If group already has 'nestedGroups' key
              state.groups[i].nestedGroups = [...state.groups[i].nestedGroups, nestedGroup ]
            }else{
              state.groups[i].nestedGroups = [nestedGroup]
            }
            break;
          }
        }
      }

      // Add New group
      state = {
        groups: [...state.groups, action.payload.newGroup]
      };

      updateGroupHandler(state);
      break;

    case 'RENAMEGROUP':
      // {id: "1", mewName: "aaaaaaaa"}
      const groupId = +action.payload.id;
      const newName = action.payload.newName;

      for(let i = 0; i < state.groups.length; i++) {
        if(state.groups[i].id === groupId){
          // Copy group and edit
          const groupToEdit = state.groups[i];
          groupToEdit.content = newName;
          // Remove group from groups
          const filteredGroups = state.groups.filter(item => item !== groupToEdit);
          // Add edited group back to the rest of the groups
          state = {
            groups: [...filteredGroups, groupToEdit]
          };
          break;
        }
      }

      updateGroupHandler(state);
      break;

    case 'UPLOADGROUP':
      state = {
        groups: action.payload
      };
      break;

    case 'REMOVEGROUP':
        const idToRemove = +action.payload;

        // Nested group stored in the array in parent group.
        // ! So, when remove Nested group, info about nesting must be removed from the parent group Arr.
        const findNested = () => { // Not in immutable way because group will be removed latter anyway and will update timeline
          for(let i = 0; i < state.groups.length; i++){
            if(state.groups[i].nestedGroups && state.groups[i].nestedGroups.includes(idToRemove)){ // check if array contains item (can be more than one nested group)
              // remove nested group from array of nested groups
              const filteredNestedGroups = state.groups[i].nestedGroups.filter(item => item !== idToRemove);
              state.groups[i].nestedGroups = filteredNestedGroups;
              // remove 'nestedGroups' key if vale(array) is empty
              if(state.groups[i].nestedGroups.length === 0){
                delete state.groups[i].nestedGroups
              }
            }
          }
        };

        // TODO: remove nested group when its parent group removed

        for(let i = 0; i < state.groups.length; i++) {
          if(state.groups[i].id === idToRemove){
            // Find if this group is nested by other
            findNested();
            // Remove group
            const filteredGroups = state.groups.filter(item => item !== state.groups[i]);
            state = {
              groups: [...filteredGroups]
            };

            break;
          }
        }

        updateGroupHandler(state);
        break;

      default:
          return state
  }
  return state;
};

export default groupReducer;

