import React from 'react';
import { Button, SideNav } from 'react-materialize';
import ColumnView from '../components/ColumnView';
import IosApps from 'react-ionicons/lib/IosApps';

const SidePanel = () => {
  return (
    <SideNav
      className="side-panel"
      id="SideNav-10"
      options={{
        draggable: true,
        edge: 'right'
      }}
      trigger={
        <Button
          className="switch-view-btn"
          floating
          icon={<IosApps />}
          large
          node="button"
          waves="light"
        />
      }
    >
    <ColumnView />
  </SideNav>
  )
};

export default SidePanel;
