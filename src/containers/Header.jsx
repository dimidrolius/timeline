import React from 'react';
import Settings from './Settings.jsx';
import Auth from '../components/Auth.jsx';
import EditGroupBtns from './EditGroupBtns';

function Header() {

  return (
    <div className="header">
      <EditGroupBtns />
      <Auth />
      <Settings />
    </div>
  )
}

export default Header;
