import React from 'react';
import { Provider } from 'react-redux';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import '../scss/style.scss';
import Container from './Container.jsx';

import store from '../store';

function App() {
  return (
    <Provider store={store}>
      <Container className="App" />
    </Provider>
  );
}

export default App;
