import React, { useState } from 'react';
import {connect} from 'react-redux';
import Timeline from 'react-visjs-timeline';
import options from '../components/Options';
import EditItem from '../components/EditItem';
import ItemInfo from '../components/ItemInfo';
import AddNewBtn from '../components/AddNewBtn';
import SidePanel from './SidePanel';

// --- Open Edit Item Modal
const editItemModal = React.createRef();
const triggerOpenModal = <span ref={editItemModal} className="hidden"></span>;


function TimelineContainer(props) {

  const [editItemData, setEditItemData] = useState();
  const [itemId, setItemId] = useState();

  const openModalClickHandler = (props) => {
    // if(!props.event.hasOwnProperty('isTrusted')){ // If click NOT on TimeLine
    if(props.event.target.className !== 'vis-group'){  // If click NOT on TimeLine, but on Item
      editItemModal.current.click(); // Trigger click to open modal
    }
    // console.log(" !!! Obj Item data to edit", props.event.firstTarget.dragCenterItem.data)
    setEditItemData(props.event); // If ut it inside "if" - bug "previously clicked item dada in new item field" 
  };

  // function zoomHendler(props) {
  //     // handle range change
  //     console.log("zoom Hendler", props.event)
  // }

  // function clickHandler(props) {
  //     // console.log("clickHandler", props.event.target ? props.event.target.firstChild : null)
  //     const clickedItem = props.event.target.dragCenterItem;
  //     setItemId(clickedItem ? clickedItem.id : null);
  // }

  const clickHandler = (props) => {
    return props.items[0] ? setItemId(props.items[0]) : null
  }

  return (
    <div>
      <EditItem trigger={triggerOpenModal} itemData={editItemData}  />
      <div className="timeline-wrap">
        <SidePanel editItemModal />
        <Timeline
          options={options}
          items={props.items}
          groups={props.groups}
          // clickHandler={clickHandler}
          // clickHandler={(e)=>{console.log(e)}}
          doubleClickHandler={openModalClickHandler}
          selectHandler={clickHandler}
          // dragoverHandler={handler}
          // rangechangeHandler={zoomHendler}
          // rightSidebarWidth={150}
          // rightSidebarContent={<div>Above The Right</div>}
          // sidebarContent={<div>Above The Left</div>}
          // itemHeightRatio={0.75}
        />
        <AddNewBtn />
        <ItemInfo itemId ={itemId} items={props.items} />
      </div>
    </div>
  )
}

function mapStateToProps(state){
  return {
    items: state.itemsReducer.items,
    groups: state.groupReducer.groups
  }
}

export default connect(mapStateToProps, null)(TimelineContainer); //, mapDispatchToProps
