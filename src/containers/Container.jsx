import React from 'react';
import {connect} from 'react-redux';
import Landing from './Landing.jsx';
import TimelineContainer from './TimelineContainer.jsx';
import InitDataFromFirebase from '../components/InitData.jsx';
import Header from './Header.jsx';
import Loader from '../components/Loader';
import Terms from '../components/Terms.jsx';

function Container(props) {
  const storeUser = props.user;

  // TODO: ? refactore it to load user from localStorage, to nod display landing on short time when reload
  return (
    <div>
      {storeUser && storeUser.email
        ?
        <div>
          <Terms />
          <Header />
          <InitDataFromFirebase />
          <TimelineContainer />
        </div>
        :
        <>
          <Loader />
          <Landing />
        </>
      }
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer.user
  }
};

export default connect(mapStateToProps, null)(Container); // mapDispatchToProps
// export default Container;
