import React from 'react';
import { Modal, Button } from 'react-materialize';
import MdCloseCircle from 'react-ionicons/lib/MdCloseCircle';
import RemoveAccount from '../components/RemoveAccount';
import Download from '../components/Download';
import Upload from '../components/Upload';
// import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined';
import MdMenu from 'react-ionicons/lib/MdMenu';

const triggerSettings = <div className="burger-menu"><MdMenu fontSize="large" /></div>;

function Settings() {
  return (
    <Modal
      actions={[
        <Button flat modal="close" node="button" waves="green"><div className="closeModalIcon"><MdCloseCircle /></div></Button>
      ]}
      bottomSheet trigger={triggerSettings}>
      <div className="settings">
        <RemoveAccount />
        <Download />
        <Upload />
      </div>
    </Modal>
  );
}

export default Settings
