import React from 'react';
import Auth from '../components/Auth.jsx';
import { phoneSize } from '../components/settings.js';
// import { Row, Col } from 'react-materialize';
// import MdMedkit from 'react-ionicons/lib/MdMedkit'
// import IosCar from 'react-ionicons/lib/IosCar'
// <div className="landing_usecases">
//   <div>
//   <h3><MdMedkit />Medical History</h3>
// <p>
// Be a holder of yours and yours children medical history to provide full information to the doctors from different clinics.
// </p>
// </div>
// <div>
//   <h3><IosCar />Car service history</h3>
//     <p>
//     Store all important events at one place.
//   </p>
// </div>
// </div>

function Landing() {
  let banner = 'banner-mobile.jpg';
  if (window.matchMedia(`(min-width: ${phoneSize}px)`).matches ) {
    banner = 'banner.jpg';
  }

  return (
    <>
      <div className="landing_description">
        <div className="banner-wrap">
          <img src={require(`../static/${banner}`)} />
        </div>  
        <h4>
        Store any information related to your health history.
        </h4>
        <p>
        Simple and free solution to digitalize all the events data, store it in one place and never worry of getting important details forgotten.
        </p>
      </div>
      <Auth />
      <div className="landing">
      <div className="landing_functions">
        <h4>Create Item</h4>
        <p>
          Create new item by double click, then click on it to edit.<br />
          Use colors to mark events depending on importance.
          Use description area to add more information.
        </p>
      </div>
      <img src={require('../static/new_item.gif')}/>
      <div className="landing_usecases_text">
        <h4>Set event Period</h4>
        <p>
          Resize item to set start and end date for the item. Move it on the timeline and among categories.
        </p>
      </div>
      <img src={require('../static/resize_item.gif')}/>
      <div className="landing_usecases_text">
        <h4>Add image link</h4>
        <p>
          Add image stored on your Google Drive.
        </p>
      </div>
      <img src={require('../static/add_image.gif')}/>
    </div>
    </>
  )
}

export default Landing;

