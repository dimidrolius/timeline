import React from 'react';
import { Dropdown, Button, Divider } from 'react-materialize';
import AddGroup from '../components/AddGroup';
import EditGroup from '../components/EditGroup.jsx';
import MdCreate from 'react-ionicons/lib/MdCreate';

const EditGroupBtns = ({ groups, onChange, selectedValue }) => {
  return (
    <div className="edit-group-btn">
      <Dropdown
        id="Dropdown_6"
        trigger={<Button node="button"><MdCreate /><span>Groups</span></Button>}
      >
        <AddGroup />
        <EditGroup />
      </Dropdown>
    </div>
  )
};

export default EditGroupBtns;

