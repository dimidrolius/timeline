import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import itemsReducer from '../reducers/itemsReducer';
import groupReducer from '../reducers/groupReducer';
import userReducer from '../reducers/userReducer';


const myLogger = (store) => (next) => (action) => {
  next(action);
};

const store = createStore(
  combineReducers({ itemsReducer, groupReducer, userReducer }),
  {},
  applyMiddleware(myLogger, createLogger()));

store.subscribe(() => {
  // console.log("Store Updated! ", store.getState());
});

export default store;
