import React from 'react';
import {connect} from 'react-redux';
import { Checkbox } from 'react-materialize';

function Terms(props) {

  var isTermsAccepted = JSON.parse(localStorage.getItem('TL_terms'));
  if(isTermsAccepted) {
    return null;
  }

  const onAcceptTerms = () => {
    props.acceptTerms(true); // Sett Redux and FireStore

    setTimeout(function(){
      localStorage.setItem('TL_terms', JSON.stringify(true));
      window.location.reload();
    }, 1000);
  };

  const termsWrap = {
    height: '100%',
    width: '100%',
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'rgba(0,0,0,0.5)',
    zIndex: 100
  };

  const termsDialog = {
    backgroundColor: '#fff',
    padding: '50px',
    display: 'flex'
  };


  return (
    <div style={termsWrap}>
      <div style={termsDialog}>
        <span>
          <Checkbox
            value="false"
            checked={false}
            onChange={onAcceptTerms}
            label=""
          />
        </span>
        <span>I have read and agree to the <a href="./terms.html" target="_blank">Privacy Policy, Terms and Conditions</a></span>
      </div>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    acceptTerms: (obj) => {
      dispatch({
        type: 'SETTERMS',
        payload: obj
      })
    }
  }
};

export default connect(null, mapDispatchToProps)(Terms);
