import { defaultItemLength } from './settings';

const timeConverter = (UNIX_timestamp) => {
  let a = new Date(UNIX_timestamp);
  let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  let year = a.getFullYear();
  let month = months[a.getMonth()];
  let date = a.getDate();
  // var hour = a.getHours();
  // var min = a.getMinutes();
  // var sec = a.getSeconds();
  return date + ' ' + month + ' ' + year ;
}

// Item will be 1 h long if no resized //TODO: add flag on resize instead?
const isDefaultItemLength = (end, start) => (
  end.getTime() - start.getTime() ===  defaultItemLength
)

export { timeConverter, isDefaultItemLength }