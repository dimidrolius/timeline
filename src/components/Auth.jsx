import React from 'react';
import PropTypes from 'prop-types';
import withFirebaseAuth from 'react-with-firebase-auth';
import 'firebase/auth';
import {connect} from 'react-redux';
import firebase from '../firebase/firebaseConfig';
import { Button } from 'react-materialize';

// https://github.com/armand1m/react-with-firebase-auth
function Auth(props) {
  console.log("--{ auth }--", props)
  const {
    user,
    signOut,
    signInWithGoogle,
    addUserData
  } = props;
    
  if(user){
    addUserData(user);
  }
    
  if(user === null){ // become null on signOut
    addUserData(null);
    localStorage.removeItem('TL_email');
  }

  return (
    <div className="user-section">
      {
        user
          ? <div className="user-info"><img src={`${user.photoURL}`} alt="user" /> <span className="user-info-text">{user.displayName}</span></div>
          : null
      }

      {
        user
          ? <Button
            node="a"
            onClick={signOut} className="sign-out"
            waves="light"
            >
            Sign Out
            </Button>
          : <Button onClick={signInWithGoogle} className="sign-in">Sign in with Google</Button>
      }
    </div>
  );
}

const firebaseAppAuth = firebase.auth();

const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider()
};

const mapDispatchToProps = (dispatch) => {
  return {
    addUserData: (obj) => {
      dispatch({
        type: 'ADDUSER',
        payload: obj
      })
    }
  }
};

Auth.propTypes = {
  // User data comes from 'firebase/auth'
  user: PropTypes.shape({
    // Photo/Icon from google account
    photoURL: PropTypes.string,
    // Name from google account
    displayName: PropTypes.string
  }),
  // firebase/auth signOut function
  signOut: PropTypes.func.isRequired,
  // firebase/auth signIn function
  signInWithGoogle: PropTypes.func.isRequired,
  // Add user data to Redux by userReducer
  addUserData: PropTypes.func.isRequired
};

Auth.defaultProps = {
  user: undefined
};

export default connect(null, mapDispatchToProps)(withFirebaseAuth({
  providers,
  firebaseAppAuth
})(Auth));
