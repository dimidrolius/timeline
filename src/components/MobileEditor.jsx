import React, { useState } from 'react';
import Editor from 'react-pell'

function TextEditor ({ getData, value }) {
  const [textValue, setTextValue] = useState(null);

  // TODO: because of state, when start typing - item Title resets to default falue
  const handleData =(html)=> {
    setTextValue(html);
    getData(html);
  };

  return (
    <Editor
      value={textValue}
      defaultContent={textValue !== null ? textValue : value}
      onChange={handleData}
      actions={
        ['bold', 'italic', 'underline', 'image', 'link', 'olist', 'line']}
    />
  );
}

export default TextEditor;
