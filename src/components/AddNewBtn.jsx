import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Button, Modal } from 'react-materialize';
import IosArrowForward from 'react-ionicons/lib/IosArrowForward';
import EditItem from '../components/EditItem';
import { defaultItemLength } from './settings';
import SelectGroup from './SelectGroup';

// Here we open modal first with Category select, than open edit Item modal by "Next" btn click

const editItemModal = React.createRef();
const triggerOpenModal = <span ref={editItemModal} className="hidden"></span>;

const AddNewBtn = ({ groups }) => {
  const [selectedGroup, setSelectedGroup] = useState();

  const handleSelectGroup = event => {
      setSelectedGroup(event.target.value.toString());
  };

  const openEditModalHandler = (props) => {
      editItemModal.current.click(); // Trigger click to open modal
  };

  const initData = {
    id: parseInt((new Date().getTime()).toFixed(0)).toString(),
    start: new Date(),
    end: new Date(new Date().getTime() + defaultItemLength), // + 1 day
    content: null,
    group: selectedGroup,
    type: 'range',
    textEditor: null,
    hideEndDate: true
  }

  // We can create new item on TimeLine click. It creates new item already with "id" and "start/end" date,
  // so we need just click on it to edit and save.
  // If we create new item by "Add New" btn, we need to add "id" and "start/end" date by yourself.
  const itemData = { // Structure to be accepted by EditItem (emulate TimeLine obj)
    firstTarget: {
      dragCenterItem: {
        data: initData
      }
    }
  }

  return (
    <>
      <EditItem trigger={triggerOpenModal} itemData={itemData} /> 
      <Modal
        className="add-new-modal"
        actions={[
          <Button
            modal="close"
            node="button"
            waves="green"
            disabled={!selectedGroup || selectedGroup === '0'}
            className="next-btn"
            onClick={openEditModalHandler}
            >
              Next <IosArrowForward />
          </Button>
        ]}
        header="Add new item"
        id="Modal-add-new-item"
        trigger={
          <Button
            className="add-btn"
            floating
            icon={'+'}
            large
            node="button"
            waves="light"
          />
        }
      >
        <SelectGroup
          groups={groups}
          onChange={handleSelectGroup}
          selectedValue={selectedGroup}
         />
      </Modal>
    </>
  )
};

const mapStateToProps = (state) => {
  return {
    groups: state.groupReducer.groups
  }
};

AddNewBtn.propTypes = {
  // Group list to select when creating an item
  groups: PropTypes.arrayOf(
    PropTypes.shape({
      // Group title
      content: PropTypes.string.isRequired,
      // Group id
      id: PropTypes.number.isRequired,
      // An array of all nested groups of this group
      nestedGroups: PropTypes.arrayOf(PropTypes.number)
    })
  ).isRequired
};

AddNewBtn.defaultProps = {
  groups: [
    {
      nestedGroups: []
    }
  ]
};

export default connect(mapStateToProps, null)(AddNewBtn);

