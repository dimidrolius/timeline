// seconds * minutes * hours * milliseconds = 1 day
// let day = 60 * 60 * 24 * 1000;
// let hour = 60 * 60 * 1000;

// const oneDay = 86400000;
const oneHour = 3600000;

const defaultItemLength = oneHour;

const sampleItem = [
  {
    id: '0',
    start: new Date(),
    end: new Date(new Date().getTime() + defaultItemLength), // + 1 day
    content: 'Sample',
    group: 1,
    type: 'range',
    textEditor: 'Time line can not be empty. Edit this item or create new one',
    hideEndDate: false,
    imgLink: null,
    color: null
  }
];
const groupToInit = [
  {
    content: 'Group A',
    id: 1,
    title: 'Group title'
  }
];

const phoneSize = 767;

export { oneHour, defaultItemLength, sampleItem, groupToInit, phoneSize } // defaultItemLength ?