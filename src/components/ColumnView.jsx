import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { timeConverter, isDefaultItemLength } from './helpers';
import parse from 'html-react-parser';
import ItemImage from './ItemImage';
import SelectGroup from './SelectGroup';

const ColumnView = ({ items, groups }) => {
  console.log("------items----", items)
  console.log("------groups----", groups)
  const [selectedGroup, setSelectedGroup] = useState();

  const handleSelectGroup = event => {
      setSelectedGroup(event.target.value.toString());
  };

  // Sort items by date before display
  const sortedItems = items.sort(function(a, b) {
    var c = new Date(a.start);
    var d = new Date(b.start);
    return c-d;
  })

  const itemColor = (item) => {
    if (item.color) {
      return {
        backgroundColor: item.color
      } 
    }
    return;
  };

  const ItemsList = () => (
    sortedItems.map(item => {
      const defaultItemLength = isDefaultItemLength(item.end, item.start);

      return (
      item.group.toString() === selectedGroup
      ? <div key={ item.id } className='column-view-item'>
          <div className="column-view-item-color" style={itemColor(item)}></div>
          <div className="column-view-item-head">
            <p className="column-view-item-title">{item.content}</p>
            {
            defaultItemLength && item.hideEndDate || item.hideEndDate
            ?
            <div>{timeConverter(item.start)}</div>
            :
            <div>{timeConverter(item.start)} - {timeConverter(item.end)}</div>
            }
          </div>

          <div className="column-view-item-description">{item.textEditor ? parse(`${item.textEditor}`) : ""}</div>
          <div className="column-view-item-img"><ItemImage image={item.imgLink} /></div>
        </div>
      : null)
    })
  );

  return (
    <>
      <SelectGroup
        groups={groups}
        onChange={handleSelectGroup}
        value={selectedGroup}
       />
      <ItemsList />
    </>
  )
};

const mapStateToProps = (state) => {
  return {
    items: state.itemsReducer.items,
    groups: state.groupReducer.groups
  }
};

ColumnView.propTypes = {
  // User data comes from 'firebase/auth'
  items: PropTypes.arrayOf(
    PropTypes.shape({
      color: PropTypes.string,
      // Title
      content: PropTypes.string,
      end: PropTypes.date,
      group: PropTypes.number,
      // Display start date only
      hideEndDate: PropTypes.bool,
      id: PropTypes.string,
      imgLink: PropTypes.string,
      start: PropTypes.date,
      textEditor: PropTypes.string,
      // Type of item. Curently it is unused prop. Type only "range",
      // and I have no functional implemented for another type selection
      type: PropTypes.string
    })
  ),

  // Group to be added to redux
  groups: PropTypes.arrayOf(
    PropTypes.shape({
      // Group title
      content: PropTypes.string.isRequired,
      // Group id
      id: PropTypes.number.isRequired,
      // An array of all nested groups of this group
      nestedGroups: PropTypes.arrayOf(PropTypes.number)
    })
  ).isRequired
};

ColumnView.defaultProps = {
  items: [],
  groups: [
    {
      nestedGroups: []
    }
  ]
};

export default connect(mapStateToProps, null)(ColumnView);
