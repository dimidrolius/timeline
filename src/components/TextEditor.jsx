import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

function TextEditor({ textLimit, getData, value }) {
  const handleEditorChange = (e) => {
    // console.log('Content was updated:', e.target.getContent());
    textLimit(e.length);
    getData(e)
  };

  let toolbarArr;
  window.innerWidth > 1024
    ?
    toolbarArr = 'undo redo | formatselect | bold italic backcolor | \
          alignleft aligncenter alignright alignjustify | \
          bullist numlist outdent indent | removeformat | help'
    :
    toolbarArr = 'bold italic backcolor | bullist numlist';

  return (
    <Editor
      apiKey="7xx3mgw7yqa26fbdi40bbktr67ao3df44ls6811zwyb5msej"
      plugins='image table'
      // initialValue="<p>This is the initial content of the editor</p>"
      value={value}
      init={{
        height: 300,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code help wordcount'
        ],
        toolbar: toolbarArr
      }}
      // onChange={handleEditorChange}
      onEditorChange={handleEditorChange}
    />
  )
}

export default TextEditor;

