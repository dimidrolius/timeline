import React, { useState } from "react";
import {connect} from 'react-redux';
import { Modal, Button } from 'react-materialize';
import EditModal from './X__EditItemModal.jsx'
// !!!!!!!!!!!!!!! --- We use another approach, not this file
// Use this file to handle "onUpdate" from "Options.js" - for native double click to add new item (don't know how to open modal)
// I use "doubleClickHandler" instead in "TimelineContainer.jsx" because I need modal
// ! Problem is thea Options.js "onUpdate" require to return function with callback, so I can't return modal component


// "onUpdate" method from Options.js require to have this function to be returned

// This component inited in "TimelineContainer.jsx" to render hidden modal on page.
// Than it invoking in "Option.js" by "onUpdate" with param to show modal.
// If "!toRet" - return modal.
// When click "Save" on modal, "toRet = true" and component returns "callback" edited data.
// Data should be displayed in Timeline automatically and saved to redux and firestore

// !!! Component must be invoked from react component to render <EditModal />
// !!! mapDispatchToProps possible only if component invoked from another react component, not from method in - "Options.js"

function EditItem(props) { // invoking with "props" only by "onUpdate"
    console.log("!!!_------------------------", props) // object

    let returnCallback = false;


    if(props === 'onUpdate'){ // Called from "Option.js"
        alert("onUpdate")
    }

    // Render hidden modal if called only by "Timeline"
    if( returnCallback === false ){
        // <button onClick={() => toRet = true}></button>
        // Show modal if called from "Option.js"
        return (
            <EditModal />
        );

    } else {
        return function (item, callback) {
            alert("callback!")
            // console.log("--Item--",item)

            // Possible solution:
            // 1. open modal with callback as parameter
            // 2. return callback onClick "Save"

            item.description = "tro-lo-lo-lo-lo"
            item.content = prompt('Edit items text:', item.content);



            if (item.content != null) {
                callback(item); // send back adjusted item
            }
            else {
                callback(null); // cancel updating the item
            }
        };
    }
}
// function EditItem(props) {
//     // modal();
//     return function (item, callback) {
//         item.description = "tro-lo-lo-lo-lo"
//         item.content = prompt('Edit items text:', item.content);
//         if (item.content != null) {
//             callback(item); // send back adjusted item
//         }
//         else {
//             callback(null); // cancel updating the item
//         }
//     };
// }

// function mapStateToProps(state){
//     console.log("mapStateToProps", state)
//     return {
//         count: state.count
//     }
// }
//
// function mapDispatchToProps(dispatch) {
//     return {
//         itemClick: ()=> {
//             const action = { type: 'ITEMCLICK' };
//             dispatch(action);
//         }
//     }
// }

export default EditItem;
// export default connect(null, mapDispatchToProps)(EditItem);