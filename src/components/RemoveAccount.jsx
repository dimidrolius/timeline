import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withFirebaseAuth from 'react-with-firebase-auth';
import 'firebase/auth';
import firebase from '../firebase/firebaseConfig';
import { Modal, Button } from 'react-materialize';
import IosTrash from 'react-ionicons/lib/IosTrash';
import MdCloseCircle from 'react-ionicons/lib/MdCloseCircle';
import { removeAccount } from '../firebase/mutations';

function RemoveAccount(props) {
  const {
    signOut,
  } = props;

  const triggerAddGroup = <Button><IosTrash color="#fff" /><span>Remove Account</span></Button>;

  const handleRemove = () => {
    removeAccount(signOut);
    localStorage.removeItem('TL_email');
    localStorage.removeItem('TL_terms');
  };

  return (
    <Modal
      actions={[
        <Button flat modal="close" node="button" waves="green"><div className="closeModalIcon"><MdCloseCircle /></div></Button>
      ]}
      trigger={triggerAddGroup} 
    >
      <>
        <p>Your Account and all Records will be removed permanently.</p>
        <Button onClick={handleRemove}>Confirm</Button>
      </>
    </Modal>
  )
}

const firebaseAppAuth = firebase.auth();

RemoveAccount.propTypes = {
  // SignOut function. Comes from firebase/auth
  signOut: PropTypes.func.isRequired
};

export default withFirebaseAuth({firebaseAppAuth})(RemoveAccount);
