import React from 'react';
import { Modal, Button } from 'react-materialize';
import MdCloseCircle from 'react-ionicons/lib/MdCloseCircle';

function ItemImage(props){

  const inputLink = props.image;

  if(!inputLink){
    return null
  }

  let imgLink;
  let imgThumb;
  // Google drive link
  if(inputLink && inputLink.includes('drive.google.com')) {
    const imgId = inputLink.split('https://drive.google.com/file/d/').pop().split('/view?usp=sharing')[0];
    // imgLink = inputLink.replace('open?', 'uc?');
    imgThumb = 'https://drive.google.com/thumbnail?id=' + imgId;
    imgLink = 'https://drive.google.com/uc?export=view&id=' + imgId;
  }else {
    // Not google drive img link
    imgLink = inputLink;
  }

  return (
    imgLink.includes('http')
      ?
      (
        <div>
          <a href="#modal-img" className="img-modal-trigger modal-trigger"><img src={imgLink} alt="attachment" /></a>
          <Modal
            id="modal-img"
            actions={[
              <Button flat modal="close" node="button" waves="green"><div className="closeModalIcon"><MdCloseCircle /></div></Button>
            ]}
          >
            <img src={imgLink} alt="attachment" />
          </Modal>
        </div>
      )
      : <div>Wrong image link</div>
  )
}

export default ItemImage;

