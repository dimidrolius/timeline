import itemTemplate from './itemTemplate';
import { editItem } from '../firebase/mutations';

let today = new Date(new Date().setHours(0,0,0,0));
let tomorrow = new Date(new Date().setHours(23,59,59,999)+1);
let oneYearFromToday = new Date(new Date(new Date().setFullYear(new Date().getFullYear() + 1)));

const options = () => {
  return {
    start: today,
    end: tomorrow,
    min: new Date(2019, 1, 1),  // lower limit of visible range
    max: oneYearFromToday,      // upper limit of visible range
    multiselect: true,
    type: 'range', // TODO: add "point" type with styles // range
    // type: 'background',
    width: '100%',
    height: window.innerHeight - 130 - 40, // - item-info - header
    // minHeight: '400px',
    stack: false,
    showMajorLabels: true,
    showCurrentTime: true,
    zoomMin: 1000000,
    // zoomMax: 1000 * 60 * 60 * 24*31*3,
    selectable: true,
    editable: {
      add: true,         // add new items by double tapping
      updateTime: true,  // drag items horizontally
      updateGroup: true, // drag items from one group to another
      // remove: true,       // delete an item by tapping the delete button top right
      overrideItems: false  // allow these options to override item.editable
    },
    format: {
      minorLabels: {
        minute: 'h:mma',
        hour: 'ha'
      }
    },
    // Native "TimeLine" onUpdate method - update item without page reload.
    // Tried to use it with react components to show edit item modal, but no luck.
    // "onUpdate" require to return function with callback, so I can't return modal react component
    // If I return non react component, I can't use mapDispatchToProps which can be invoked only from another react component
    // mapDispatchToProps needed to store data from modal to Redux


    // onUpdate: ()=> {
    //     var result = EditItem('onUpdate');
    //     console.log("-------- onUpdate --------", result)
    //     // if( typeof result === 'function'){
    //     //
    //     // } else {
    //     //     return
    //     // }
    // },
    // onUpdate: EditItem(), // Native double click handler

    // onUpdate: function (item, callback) {
    //     alert("ddd")
    //     console.log("---- Item ----", item)
    //
    //
    //     // Get data from redux store and uppdate item
    //
    //     // item.description = "tro-lo-lo-lo-lo"
    //     // item.content = prompt('Edit items text:', item.content);
    // //
    // // // Try to invoke modal here >>>
    // //
    // //     if (item.content != null) {
    // //         callback(item); // send back adjusted item
    // //     }
    // //     else {
    // //         callback(null); // cancel updating the item
    // //     }
    // },

    template: function (item, element, data) {
      return itemTemplate(item, data)
    },

    // onMoving: function (item, callback) {
    //   // item.moving = true;
    //     console.log("--- Moving ", item)
    // }
    
    onMove: function (item, callback) {
      editItem(item)
    }
    // onMoveGroup: function (item, callback) {
    //   console.log(item)
    // }
  };
};

// export let updateItem = options().onUpdate;
export default options();
