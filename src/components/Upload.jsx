import React, { useState } from 'react';
import {connect} from 'react-redux';
import { Button } from 'react-materialize';
import IosUpload from 'react-ionicons/lib/IosCloudUpload';

const Upload = ({uploadGroups, uploadItems }) => {
  const dofileUpload = React.createRef();

  const [fileName, setFileName] = useState("");

  const upload = (e) => {
  	e.preventDefault();
    dofileUpload.current.click();
  }
  
  const openFile = (evt) => {
      const fileObj = evt.target.files[0];
      const reader = new FileReader();
          
      let fileloaded = e => {
        // e.target.result is the file's content as text
        const fileContents = JSON.parse(e.target.result);

        // 1. Update group in storage
        uploadGroups(fileContents.groups);
        uploadItems(fileContents.items);
        // 2. Update items in storage
        // 3. Import "Upload" from mutation.js and call it here to save data to Firebase
      }
      setFileName(evt.target.files[0].name)
      
      // Mainline of the method
      reader.onload = fileloaded;
      reader.readAsText(fileObj);  
  }
  
  return (
    <div>
      <form>
        <Button onClick={upload}>Select</Button>
        <Button  disabled={!fileName.length}><IosUpload color="#fff" />Upload</Button>
        <div>{ fileName }</div>
        <div>{ fileName.length ? "Existing data will be removed!" : null }</div>

        <input type="file" className="hidden"
          multiple={false}
          accept=".json,application/json"
          onChange={evt => openFile(evt)}
          ref={dofileUpload}
        />
      </form>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadGroups: (groups) => {
      dispatch({
        type: 'UPLOADGROUP',
        payload: groups
      })
    },
    uploadItems: (items) => {
      dispatch({
        type: 'UPLOADITEMS',
        payload: items
      })
    }
  }
};

export default connect(null, mapDispatchToProps)(Upload);
