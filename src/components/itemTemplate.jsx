function Item(item, data) {
  return (
    `<div ${data.color ? 'style="border-left: 3px solid'+ data.color + '"' : ''}>
      <p>${item && item.content}</p>
    </div>`
  );
}

export default Item;
