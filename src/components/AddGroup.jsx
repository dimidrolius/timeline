import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, TextInput, Select } from 'react-materialize';
import {connect} from 'react-redux';
import MdAddCircle from 'react-ionicons/lib/MdAddCircle'
import MdCloseCircle from 'react-ionicons/lib/MdCloseCircle';

function AddGroup(props) {
  const [toggleModal, setToggleModal] = useState({
    isModalOpen: false
  });

  const [formGroup, setFormGroup] = useState({
    id: null,
    groupName: '',
    selectedParentGroupId: 0
  });

  const isBtnEnabled = formGroup.groupName ? formGroup.groupName.length > 0 : null;
  const triggerAddGroup = <Button><MdAddCircle color="#fff" /><span>Add Group</span></Button>;
  const storeGroups = props.groups; // all groups from redux // TODO: may be wrong way to copy store
  // const storeGroups = JSON.parse(JSON.stringify(props.groups));
  // const storeGroups = [...props.groups];
  // const storeGroups = Object.assign(props.groups);

  const newGroup = {};
  const parentGroup = {};
  const groupsToDispatch = {};

  // Add new group name to state
  const handleGroupName = groupName => event => {
    setFormGroup({ ...formGroup, [groupName]: event.target.value });
    setToggleModal({ isModalOpen: true }); // Prevent modal to close when input
  };

  // Add parent group id to state
  const handleSelectParentGroup = e => {
    e.persist();
    setFormGroup(oldValues => ({
      ...oldValues,
      selectedParentGroupId: e.target.value
    }));
    setToggleModal({ isModalOpen: true }); // Prevent modal to close when select
  };

  const handleSaveGroup = (e) => {
    e.preventDefault();

    // Generate id for new group, Add id to state
    let maxGroupId = 0;
    storeGroups.map(group => (
      group.id >= maxGroupId ? maxGroupId = +group.id+1 : null
    ));
    setFormGroup(formGroup.id = maxGroupId); //TODO: Check if this correct

    // const addNewGroupId = id => {
    //     setFormGroup({ ...formGroup, [id]: maxGroupId });
    // };
    // addNewGroupId('id');

    // Form new group
    newGroup.id = formGroup.id;
    newGroup.content = formGroup.groupName;
    groupsToDispatch.newGroup = newGroup;

    // Form group selected as parent
    if(formGroup.selectedParentGroupId){
      parentGroup.id = formGroup.selectedParentGroupId;
      parentGroup.nestedGroup = formGroup.id;
      groupsToDispatch.parentGroup = parentGroup;
    }

    props.addNewGroup(groupsToDispatch);

    setToggleModal({ isModalOpen: false })
  };

  // TODO: add validation for parent select. After group rename, parent group in the list changes only if group name is not empty
  return (
    <Modal
      actions={[
        <Button flat modal="close" node="button" waves="green"><div className="closeModalIcon"><MdCloseCircle /></div></Button>
      ]}
      trigger={triggerAddGroup} open={toggleModal.isModalOpen}
    >
      <form>

        <TextInput
          onChange={handleGroupName('groupName')}
          label="Group Name"
          value={formGroup.groupName || ''}
        />

        <Select onChange={handleSelectParentGroup} label="Parent Group" value={formGroup.selectedParentGroupId || "0"}>
          <option value="0" >
            No parent
          </option>

          {storeGroups.map(group => (
            <option value={group.id} key={group.id}>
              {group.content}
            </option>
          ))}
        </Select>

        <Button disabled={!isBtnEnabled} onClick={handleSaveGroup}>Add Group</Button>
      </form>
    </Modal>
  )
}


const mapStateToProps = (state) => {
  return {
    groups: state.groupReducer.groups
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    addNewGroup: (obj) => {
      dispatch({
        type: 'ADDGROUP',
        payload: obj
      })
    }
  }
};

AddGroup.propTypes = {
  // Function from GroupReducer for adding new group to redux state
  addNewGroup: PropTypes.func.isRequired,

  // Group to be added to redux
  groups: PropTypes.arrayOf(
    PropTypes.shape({
      // Group title
      content: PropTypes.string.isRequired,
      // Group id
      id: PropTypes.number.isRequired,
      // An array of all nested groups of this group
      nestedGroups: PropTypes.arrayOf(PropTypes.number)
    })
  ).isRequired
};

AddGroup.defaultProps = {
  groups: [
    {
      nestedGroups: []
    }
  ]
};

export default connect(mapStateToProps, mapDispatchToProps)(AddGroup);
