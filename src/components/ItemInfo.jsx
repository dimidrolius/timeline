import React from 'react';
import ItemImage from './ItemImage';
import { timeConverter, isDefaultItemLength } from './helpers';

function ItemInfo(props) {
  const itemId = props.itemId;
  const items = props.items;
  let itemToDisplay = {};

  if(!itemId){
    return null
  }

  for (let item of items) {
    if(item.id === itemId){
      itemToDisplay = item;
      break;
    }
  }

  if (itemToDisplay.start) {
    const defaultItemLength = isDefaultItemLength(itemToDisplay.end, itemToDisplay.start);

    return (
      <>
      <div className="item-info">
        <div className="content">
          <h5>{itemToDisplay.content}</h5>
          {
            defaultItemLength && itemToDisplay.hideEndDate || itemToDisplay.hideEndDate
              ?
              <p className="content-date">{timeConverter(itemToDisplay.start)}</p>
              :
              <>
              <p className="content-date">{timeConverter(itemToDisplay.start)} - {timeConverter(itemToDisplay.end)}</p>
              </>
          }
        </div>
        <ItemImage image={itemToDisplay.imgLink} />
      </div>
      <div className="textEitorContent" dangerouslySetInnerHTML={{__html: itemToDisplay.textEditor}} />
      </>
    );
  }
  return null;
}

export default ItemInfo;
