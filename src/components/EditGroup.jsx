import React, { useState } from 'react';
import {connect} from 'react-redux';
import { Modal, Button, TextInput, Select, Row, Col } from 'react-materialize';
import MdCreate from 'react-ionicons/lib/MdCreate';
import MdCloseCircle from 'react-ionicons/lib/MdCloseCircle';

const triggerSettings = <Button><MdCreate color="#fff" /><span>Edit Group</span></Button>;

function EditGroup(props) {
  const [state, setState] = useState({
    id: 0,
    newName: ''
  });
  const [toggleModal, setToggleModal] = useState({
    isModalOpen: false
  });

  const storeGroups = props.groups;

  const isAllFieldsSelected = state.id ? state.id !== '0'  && state.newName.length > 0 : null;
  const isGroupSelected = state.id ? state.id !== '0' : null;

  const handleSelectGroup = id => event => {
    setState({ ...state, [id]: event.target.value });
    setToggleModal({ isModalOpen: true }); // prevent close when select
  };

  const handleGroupName = newName => event => {
    setState({ ...state, [newName]: event.target.value });
  };

  const handleRenameGroup = (e) => {
    e.preventDefault();

    const groupToDispatch = {
      id: state.id,
      newName: state.newName
    };

    props.editGroup(groupToDispatch);
  };

  const handleRemoveGroup = (e) => {
    e.preventDefault();
    props.removeGroup(state.id);
    props.removeGroupItems(state.id);

    setToggleModal({ isModalOpen: false })
  };

  return (
    <Modal
      actions={[
        <Button flat modal="close" node="button" waves="green"><div className="closeModalIcon"><MdCloseCircle /></div></Button>
      ]}
      header="Rename Group" trigger={triggerSettings} open={toggleModal.isModalOpen}
    >
      <form>
        <Select onChange={handleSelectGroup('id')} label="" value={state.id || ''}>
          <option value="0" >
            Select Group
          </option>
          {storeGroups.map(group => (
            <option value={ group.id } key={ group.id }>
              { group.content }
            </option>
          ))}
        </Select>

        <TextInput
          onChange={handleGroupName('newName')}
          label="New Group Name"
          value={state.newName || ''}
          maxLength="100"
        />

        <Row>
          <Col s={12} m={3}>
            <Button disabled={!isAllFieldsSelected} onClick={handleRenameGroup}>Rename</Button>
          </Col>
          <Col s={12} m={3}>
            <Button disabled={!isGroupSelected} onClick={handleRemoveGroup}>Remove&nbsp;Group&nbsp;with&nbsp;Items</Button>
          </Col>
        </Row>
      </form>
    </Modal>
  )
}

const mapStateToProps = (state) => {
  return {
    groups: state.groupReducer.groups
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    editGroup: (obj) => {
      dispatch({
        type: 'RENAMEGROUP',
        payload: obj
      })
    },
    removeGroup: (id) => {
      dispatch({
        type: 'REMOVEGROUP',
        payload: id
      })
    },
    removeGroupItems: (id) => {
      dispatch({
        type: 'REMOVEGROUPITEMS',
        payload: id
      })
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditGroup);
