// https://daveceddia.com/open-modal-in-react/

import React, { useState } from "react";
import PropTypes from 'prop-types';

function Modal(props)  {
    // const [ isOpen, setIsOpen ] = React.useState();
    //
    //     if(props.isOpen){
    //         setIsOpen(true)
    //     }else {
    //         setIsOpen(false)
    //     }
            
        // Render nothing if the "show" prop is false
    //     if(!props.isOpen) {
    //         return null;
    //     }


        const backdropStyle = {
            position: 'fixed',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            border: '1px solid #f00',
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 50,
            zIndex: 100
        };
        const modalStyle = {
            backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 500,
            minHeight: 300,
            margin: '0 auto',
            padding: 30
        };

    return (
        <div style={backdropStyle}>
            <div style={modalStyle}>
                <h2>Modal!</h2>
                {props.children}
                <div className="footer">
                    <button onClick={props.onClose}>
                        Close
                    </button>
                </div>
            </div>
        </div>
    );
    //     return (
    //         <div className="backdrop" style={{backdropStyle}}>
    //             <div className="modal" style={{modalStyle}}>
    //                 {props.children}
    //
    //                 <div className="footer">
    //                     <button onClick={props.onClose}>
    //                         Close
    //                     </button>
    //                 </div>
    //             </div>
    //         </div>
    //     );

}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    children: PropTypes.node
};

export default Modal;