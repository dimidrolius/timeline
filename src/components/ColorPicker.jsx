import React, { useState } from 'react';
import PropTypes from 'prop-types';

function ColorPicker({
  colorHandler, selected
}) {
  const [color, setColor] = useState(selected);

  const colorsArr = ['#6e67d3', '#6eacd4', '#8ec05a', '#dbd341', '#dd522f'];

  const handleColor = (e) => {
    let color =  e.currentTarget.dataset.id;
    colorHandler(color); // Return color to "EditItem" to store
    setColor(color);
  };

  let palette = [
    <div // no color Item
      key='no-color'
      data-id='transparent'
      className="palette-item"
      style={{border: '1px solid #9e9e9e'}}
      onClick={handleColor}
    >
    </div>
  ];

  for (let i in colorsArr) {
    palette.push(
      <div
        key={i}
        data-id={colorsArr[i]}
        className="palette-item"
        style={{backgroundColor: colorsArr[i]}}
        onClick={handleColor}
      >
      </div>)
  }

  return (
    <div className="palette">
      {palette}
      <div
        className="selected"
        style={{backgroundColor: color || selected}}>
      </div>
    </div>
  )
}

ColorPicker.propTypes = {
  // Select color function
  colorHandler: PropTypes.func.isRequired,
  // Selected color
  selected: PropTypes.string,
};

ColorPicker.defaultProps = {
  selected: null
};

export default ColorPicker;
