import React, { useState, useEffect } from 'react';

// This component is used to hide (overlap) the landing page on every page reload,
// before checking if the user is logged in.
// If user not logged in - loader disappeared after 2 sec.
// If user logged in - in Container.jsx ternary loads "landing" with loader first and when init data comes, it loads app

const Loader = () => {
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setVisible(false);
    }, 2000);
    return () => clearTimeout(timer);
  }, []);

  const LoaderWrapper = () => {
    if(visible){
      return (
        <div className="loader">
          <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
      )
    }else {
      return null;
    }

  };

  return (
    <LoaderWrapper />
  )
};

export default Loader;
