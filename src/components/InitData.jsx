import React from 'react';
import {connect} from 'react-redux';
import { getAllGrous, updateGroups, addItem, getAllItems } from '../firebase/mutations';
import { sampleItem, groupToInit } from '../components/settings';

function InitFBData(props) {
  const getGroup = async () => {
    let fireStoreGroups = await getAllGrous();

    if (fireStoreGroups && fireStoreGroups.length !== 0) {
      props.loadAllGroups(fireStoreGroups);
    }else {
      props.loadAllGroups(groupToInit);
      
      // // Save init group to server
      updateGroups(groupToInit); // !!! this line Remove all existing data on server in Group
      // // Save init item to server
      addItem(sampleItem[0]);
    }
  };
    
  const getItems = async () => {
    let fireStoreItems = await getAllItems();

    if (fireStoreItems && fireStoreItems.length !== 0) {
      props.loadAllItems(fireStoreItems);
    }else { // TimeLine doesn't load if no item exist, so need to load one sample item
      props.loadAllItems(sampleItem);
    }
  };

  getGroup();
  getItems();

  return (
    <div></div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadAllGroups: (obj) => {
      dispatch({
        type: 'LOADGROUPS',
        payload: obj
      })
    },
    loadAllItems: (obj) => {
      dispatch({
        type: 'LOADITEMS',
        payload: obj
      })
    },
    saveInitItem: (obj) => {
      dispatch({
        type: 'ADDITEM',
        payload: obj
      })
    },
    addInitGroup: (obj) => {
      dispatch({
        type: 'ADDINITGROUP',
        payload: obj
      })
    }
  }
};

export default connect(null, mapDispatchToProps)(InitFBData)
