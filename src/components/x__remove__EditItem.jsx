import React, { useState, useEffect } from 'react';
import { Modal, Button, TextInput, Checkbox, Row, Col } from 'react-materialize';
import {connect} from 'react-redux';
import MdTrash from 'react-ionicons/lib/MdTrash'
import TextEditor from './TextEditor.jsx';
// import MobileEditor from './MobileEditor.jsx';
import ColorPicker from './ColorPicker.jsx';

function EditItem(props){
  const [state, setState] = useState({
    content: null,
    imgLink: null,
    textEditor: null,
    color: null,
    hideEndDate: true // TODO: implement false on item resizing
  });
  const [textLimit, setTextLimit] = useState(false);
  // const [isHideEndDate, setIsHideEndDate] = useState(true);


  // Get existing in Firestore data
  let editItemData= {}; // Data from clicked Item
  // Get data existing in fireStore item and assign it to inputs value
  if(props.itemData){ // prevent error when first load
    if(props.itemData.firstTarget){ // Item exist - edit mode
      editItemData = {...props.itemData.firstTarget.dragCenterItem.data};
    } // else {} // New Item - create mode. No logic needed. Empty item added by TimeLine, just need to edit and save
    // const item = props.itemData.firstTarget.dragCenterItem.data;
  } // else {}
  // console.log("-- NO DATA from clicked item, first load", props)

  const handleItemInput = inputName => e => {
    if (inputName === 'hiddenEndDate') {
      setState({ ...state, [inputName]: !state.hideEndDate });
    }else {
      setState({ ...state, [inputName]: e.target.value });
    }
  };

  console.log("----------editItemData-----", editItemData)

  const removeItemHandler = (e) => {
    e.preventDefault();
    props.removeItem(editItemData.id);
  };

  const textEditorHandler = (e) => {
    // const editorContent = e.target.getContent(); // by onChange
    setState({ ...state, textEditor: e });
  };

  const setColor = (color) => {
    setState({ ...state, color: color });
  };

  // TODO: to refactor using useEffect?
  // Init Edited item data
  // Workaround because init state by editItemData doesn't work
  let initContent = state.content !== null ? state.content : editItemData.content;
  let initImgLink = state.imgLink !== null ? state.imgLink : editItemData.imgLink;
  let initTextEditor = editItemData.textEditor;
  let initColor = editItemData.color;

  // useEffect(() => {
  //   console.log("--------uppdate--------", props.itemData.firstTarget.dragCenterItem.data)
  //   // handleItemInput('hiddenEndDate') editItemData.hideEndDate
  // }, [props.itemData]);


  // Save data to Firestore
  const saveEditedItemHandler = (e) => {
    e.preventDefault();

    // Form item to dispatch
    const newItem = {};

    newItem.id = editItemData.id;
    newItem.start = editItemData.start;

    // Adding 1 hour range for newly created item.
    // Because by default, "end" date can be very far, depends of the current zoom
    // If item will be resized !!!After First Save, the new end date will be saved
    if ('color' in editItemData) {
      // Item has been created before
      newItem.end = editItemData.end;
    }else {
      // Item opened first time, no 'color' key exist yet
      // seconds * minutes * hours * milliseconds = 1 day
      // let day = 60 * 60 * 24 * 1000;
      let hour = 60 * 60 * 1000;
      // get object (timestamp) + 1 day
      newItem.end = new Date(editItemData.start.getTime() + hour);
    }
    newItem.group = editItemData.group;
    newItem.content = state.content || editItemData.content;
    newItem.imgLink = state.imgLink !== null ? state.imgLink : editItemData.imgLink || '';
    newItem.textEditor = state.textEditor !== null ? state.textEditor : editItemData.textEditor || '';
    newItem.color = state.color || editItemData.color || '';
    newItem.hideEndDate = state.hideEndDate; // || editItemData.hideEndDate;

    props.saveEditedItem(newItem);
  };


  // Validation
  const textLimitHandler = (textSize) => {
    textSize > 2000 ? setTextLimit(true) : setTextLimit(false);
  };
  const isTitle = state.content !== '';

  // let hideEndDate = true;

  return(
    <Modal header="Edit Item" trigger={props.trigger} className="edit-item">
      <div className="first-row">
        <TextInput
          onChange={handleItemInput('content')}
          label="Title"
          value={initContent || ''}
          maxLength="100"
        />
        <ColorPicker colorHandler={setColor} selected={initColor} />
      </div>
      <div className="second-row">
        <TextInput
          onChange={handleItemInput('imgLink')}
          label="Image link"
          value={initImgLink || ''}
          maxLength="200"
        />
        <div className="hide-end-date">
          <Checkbox
            id="Checkbox_hide-end-date"
            label="hide end date"
            value={editItemData.hideEndDate}
            checked={state.hideEndDate}
            onChange={handleItemInput('hiddenEndDate')}//setIsHideEndDate(!isHideEndDate)} // TODO: add false if resized
          />
        </div>
      </div>

      <TextEditor value={initTextEditor} textLimit={textLimitHandler} getData={textEditorHandler} />
      
      <Row>
        <Col s={6} m={3}>
          <Button onClick={removeItemHandler} className="removeItemBtn">
            <MdTrash className="iconInBtn" /> Remove
          </Button>
        </Col>
        <Col s={6} m={9}>
          <Button disabled={!isTitle || textLimit} onClick={saveEditedItemHandler} className="saveItemBtn">Save</Button>
        </Col>
      </Row>

      {textLimit ? <div className="text-limit">Max character limit has been reached</div> : null}
    </Modal>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveEditedItem: (obj) => {
      dispatch({
        type: 'ADDITEM',
        payload: obj
      })
    },
    removeItem: (obj) => {
      dispatch({
        type: 'REMOVEITEM',
        payload: obj
      })
    }
  }
};

export default connect(null,mapDispatchToProps)(EditItem);



// {window.innerWidth > 1024
//   ?
//   <TextEditor value={initTextEditor} textLimit={textLimitHandler} getData={textEditorHandler} />
//   :
//   <MobileEditor value={initTextEditor} getData={textEditorHandler} />
// }
