import React, { useState, useEffect } from 'react';
import { Modal, Button, TextInput, Row, Col, Switch } from 'react-materialize';
import {connect} from 'react-redux';
import MdTrash from 'react-ionicons/lib/MdTrash';
import MdCloseCircle from 'react-ionicons/lib/MdCloseCircle';
import { oneHour } from './settings';
import TextEditor from './TextEditor.jsx';
// import MobileEditor from './MobileEditor.jsx';
import ColorPicker from './ColorPicker.jsx';

function EditItem(props){
  const [state, setState] = useState({
    content: null,
    imgLink: null,
    textEditor: null,
    color: null,
    hideEndDate: true
  });
  const [textLimit, setTextLimit] = useState(false);

  // Init from Firestore
  useEffect(() => {
    if(props.itemData){ // prevent error when first load
      if(props.itemData.firstTarget) { // Item exist - so edit mode
        const storedItem = props.itemData.firstTarget.dragCenterItem.data;

        if ('color' in storedItem) {
          // Item has been created before
          setState({ ...state, ...storedItem })
        }else {
          // Adding 1 hour range for newly created item.
          // Because by default, "end" date can be very far, depends of the current zoom
          // If item will be resized !!!After First Save, the new end date will be saved

          // Item opened first time, no 'color' key exist yet
 
          setState({
            ...state,
            ...storedItem,
            end: new Date(storedItem.start.getTime() + oneHour) // get object (timestamp) + 1 day
          })
        }
      } // else {} // New Item - create mode. No logic needed. Empty item added by TimeLine, just need to edit and save
    }
  }, [props.itemData]);

  // Save data to Firestore
  const saveEditedItemHandler = (e) => {
    e.preventDefault();
    props.saveEditedItem(state);
  };

  const removeItemHandler = (e) => {
    e.preventDefault();
    props.removeItem(state.id);
  };

  // Validation
  const textLimitHandler = (textSize) => {
    textSize > 2000 ? setTextLimit(true) : setTextLimit(false);
  };
  const isTitle = state.content !== ''; // TODO: apply

  return(
    <Modal
      actions={[
        <Button flat modal="close" node="button" waves="green"><div className="closeModalIcon"><MdCloseCircle /></div></Button>
      ]}
      header="Edit Item" trigger={props.trigger} className="edit-item">
      <div className="first-row">
        <TextInput
          onChange={(e) => setState({...state, content: e.target.value})}
          label="Title"
          value={state.content}
          maxLength="100"
        />
        <ColorPicker colorHandler={(color)=> setState({...state, color: color})} selected={state.color} />
      </div>

      <div className="second-row">
        <TextInput
          onChange={(e) => setState({...state, imgLink: e.target.value})}
          label="Image link"
          value={state.imgLink || ''}
          maxLength="200"
        />
        <div className="hide-end-date">
          <spam className="hide-end-date-label">Hide end date</spam>
          <Switch
            id="Switch-11"
            offLabel="Show"
            onChange={() => setState({...state, hideEndDate: !state.hideEndDate})}
            onLabel="Hide"
            checked={state.hideEndDate}
          />
        </div>
      </div>

      <TextEditor value={state.textEditor} textLimit={textLimitHandler} getData={(e) => setState({...state, textEditor: e})} />
    
      <Row>
        <Col s={6} m={3}>
          <Button onClick={removeItemHandler} className="removeItemBtn">
            <MdTrash className="iconInBtn" /> Remove
          </Button>
        </Col>
        <Col s={6} m={9}>
          <Button onClick={saveEditedItemHandler} className="saveItemBtn">Save</Button>
        </Col>
      </Row>
    </Modal>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveEditedItem: (obj) => {
      dispatch({
        type: 'ADDITEM',
        payload: obj
      })
    },
    removeItem: (obj) => {
      dispatch({
        type: 'REMOVEITEM',
        payload: obj
      })
    }
  }
};

export default connect(null, mapDispatchToProps)(EditItem);

// {window.innerWidth > 1024
//   ?
//   <TextEditor value={initTextEditor} textLimit={textLimitHandler} getData={textEditorHandler} />
//   :
//   <MobileEditor value={initTextEditor} getData={textEditorHandler} />
// }
