import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'react-materialize';

const SelectGroup = ({ groups, onChange, selectedValue }) => {
  return (
    <Select className="col-view_select-category" onChange={onChange} label="" value={selectedValue}>
      <option value="0" >
        Select Group
      </option>
      {groups.map(group => (
        <option value={ group.id } key={ group.id }>
          { group.content }
        </option>
      ))}
    </Select>
  )
};

SelectGroup.propTypes = {
  groups: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  selectedValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};

SelectGroup.defaultProps = {
  selectedValue: undefined
};

export default SelectGroup;
