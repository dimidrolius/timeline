import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import 'firebase/auth';
import { Button } from 'react-materialize';
import IosDownload from 'react-ionicons/lib/IosCloudDownload';

const Download = ({ items, groups }) => {
  const handleDownload = () => {
    let arrDownload ={
      items: items,
      groups: groups
    };
    let data = JSON.stringify(arrDownload);
    console.log("---QQQQ---", data)
    const blob = new Blob([data]);
    const fileDownloadUrl = URL.createObjectURL(blob);
    const today = new Date();
    const todayDate = (today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear());

    // Create html element
    const link = document.createElement('a');
    link.href = fileDownloadUrl;
    link.setAttribute(
      'download',
      `${todayDate}_timeLine.json`,
    );

    // Append to html link element page
    document.body.appendChild(link);
    // Start download
    link.click();
    // Clean up and remove the link
    link.parentNode.removeChild(link);
  };

  return (
    <Button onClick={handleDownload}><IosDownload color="#fff" /><span>Download</span></Button>
  )
}

const mapStateToProps = (state) => {
  return {
    items: state.itemsReducer.items,
    groups: state.groupReducer.groups
  }
};

Download.propTypes = {
  // User data comes from 'firebase/auth'
  items: PropTypes.arrayOf(
    PropTypes.shape({
      color: PropTypes.string,
      // Title
      content: PropTypes.string,
      end: PropTypes.date,
      group: PropTypes.number,
      // Display start date only
      hideEndDate: PropTypes.bool,
      id: PropTypes.string,
      imgLink: PropTypes.string,
      start: PropTypes.date,
      textEditor: PropTypes.string,
      // Type of item. Curently it is unused prop. Type only "range",
      // and I have no functional implemented for another type selection
      type: PropTypes.string
    })
  ),

  groups: PropTypes.arrayOf(
    PropTypes.shape({
      // Group title
      content: PropTypes.string.isRequired,
      // Group id
      id: PropTypes.number.isRequired,
      // An array of all nested groups of this group
      nestedGroups: PropTypes.arrayOf(PropTypes.number)
    })
  ).isRequired
};

Download.defaultProps = {
  items: [],
  groups: [
    {
      nestedGroups: []
    }
  ]
};

export default connect(mapStateToProps, null)(Download);
