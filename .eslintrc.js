module.exports = {
  "env": {
      "browser": true,
      "es6": true
  },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
      // "airbnb"
        // // "plugin:prettier/recommended",
        // // "prettier/flowtype",
        // // "prettier/react",
        // "prettier/standard"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
        // "prettier"
    ],
    "rules": {
        "indent": [
            "error",
            2
        ],
        "quotes": [
            "error",
            "single"
        ]
        // "prettier/prettier": ["error", {"singleQuote": true, "parser": "flow"}]
    }
};
